use std::borrow::Cow;
use std::{fs::read_to_string, io, net::SocketAddr, path::Path};

use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone)]
pub struct Config {
    /// IP address and TCP port to bind the listener to
    pub listen_address: SocketAddr,
    /// Name of the cookie that contains the JWT token
    pub auth_cookie_name: Cow<'static, str>,
    /// JWT decoding key key
    pub secret: Cow<'static, str>,
    /// Podman socket path
    pub socket_path: Cow<'static, str>,
    /// Rust-code-server image name
    pub rcs_image: Cow<'static, str>,
}

impl Default for Config {
    fn default() -> Self {
        Self {
            listen_address: SocketAddr::from(([127, 0, 0, 1], 8000)),
            auth_cookie_name: Cow::Borrowed("access_token"),
            secret: Cow::Borrowed("demosecret"),
            socket_path: Cow::Borrowed("unix:///run/user/1000/podman/podman.sock"),
            rcs_image: Cow::Borrowed(
                "registry.gitlab.com/otafablab/rust-code-server/rust-code-server:latest",
            ),
        }
    }
}

#[derive(Debug)]
pub enum OpenConfigError {
    ReadError(io::Error),
    ParseError(toml::de::Error),
}

impl Config {
    /// Opens and parses the TOML configuration file at `path`.
    ///
    /// # Errors
    ///
    /// - If the file is inaccessible or reading it fails, a [`OpenConfigError::ReadError`] is returned
    /// - If parsing the file fails due to syntax or missing fields, a [`OpenConfigError::ParseError`] is returned
    pub fn open(path: impl AsRef<Path>) -> Result<Self, OpenConfigError> {
        let contents = read_to_string(path).map_err(OpenConfigError::ReadError)?;
        toml::from_str(&contents).map_err(OpenConfigError::ParseError)
    }
}
