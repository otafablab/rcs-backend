use crate::config::Config;
use crate::utils::wait_for_http_service;
use http::uri::Authority;
use podman_api::api::Container;
use podman_api::models::InspectContainerData;
use podman_api::opts::ContainerCreateOpts;
use podman_api::opts::VolumeCreateOpts;
use podman_api::Podman;
use unidecode::unidecode;

/// Transliterates a string into a restricted ASCII-only character
/// set. Heavy lifting done in [`unidecode::unidecode`].
pub fn transliterate(s: impl AsRef<str>) -> String {
    unidecode(s.as_ref())
        .chars()
        .map(|c| match c {
            '@' => '_',
            c => c,
        })
        .filter(|&c| c.is_alphanumeric() || "_.-".contains(c))
        .collect()
}

fn get_host_port(details: &InspectContainerData) -> String {
    details
        .network_settings
        .clone()
        .unwrap()
        .ports
        .unwrap()
        .get("3000/tcp")
        .unwrap()[0]
        .host_port
        .clone() // TODO fix
        .unwrap()
}

/// Idempotently starts the rust-code-server instance for the use
/// identified by their email. Returns the an [`Authority`] specifying
/// the host and port of the server instance.
///
/// On first run:
///
/// - a volume named by the `email`
/// ([`transliterated`](transliterate)) is created
///
/// - then a rust-code-server container is started which mounts the
/// volume into `/home/workspace`. The container publishes a random
/// port for connecting to the vscode (returned in the
/// `Authority`). The container's name the same as the volume's.
///
/// # Panics
///
/// The function will panic if:
///
/// - the [`Config::socket_path`] is invalid
///
/// - it cannot connect to the socket
///
/// - it cannot create the volume or the container
pub async fn start_rcs(email: &str, config: &Config) -> Authority {
    let email = transliterate(email);
    let podman = Podman::new(&config.socket_path).unwrap();

    let volumes = podman.volumes();
    let volume = volumes.get(email.as_str());

    let volume_opts = VolumeCreateOpts::builder()
        .name(&email)
        // doesn't work on podman 3 or something
        // // uid and gid are of the openvscode server user
        // // found this gold from https://docs.podman.io/en/latest/markdown/podman-volume-create.1.html#examples
        // .options([("o", "uid=1000,gid=1000")])
        .build();
    println!("trying to inspect volume");
    match volume.inspect().await {
        Ok(_) => {}
        Err(podman_api::Error::Error(podman_api::conn::Error::Fault { code, .. }))
            if code.as_u16() == 404 =>
        {
            volumes.create(&volume_opts).await.unwrap();
        }
        Err(e) => {
            panic!("{:?}", e);
        }
    }

    // FIXME wyhyyy
    // FIXME nightly only
    if let Err(e) = std::os::unix::fs::chown(
        &format!("/var/lib/containers/storage/volumes/{}/_data", email),
        Some(1000),
        Some(1000),
    ) {
        eprintln!("chown error {e:?}");
    }

    let containers = podman.containers();
    let container = containers.get(email.as_str());
    println!("trying to inspect container");
    let authority = match container.inspect().await {
        Ok(details) => {
            let port = get_host_port(&details);
            Authority::try_from(format!("127.0.0.1:{}", port)).unwrap()
        }
        Err(podman_api::Error::Error(podman_api::conn::Error::Fault { code, .. }))
            if code.as_u16() == 404 =>
        {
            let opts = ContainerCreateOpts::builder()
                .image(&config.rcs_image)
                .name(&email)
                .publish_image_ports(true)
                // volumes only work currently with https://gitlab.com/otafablab/podman-api-rs version 0.2.4-alpha.0
                .volumes(vec![podman_api::models::NamedVolume {
                    name: Some(email.clone()),
                    dest: Some("/home/workspace".into()),
                    options: None,
                }])
                .add_capabilities(["CAP_AUDIT_WRITE", "CAP_SYS_PTRACE"])
                .build();

            println!("trying to create container");
            let ctr = match containers.create(&opts).await {
                Ok(ctr) => Container::new(podman, ctr.id),
                Err(e) => panic!("{:?}", e),
            };
            println!("created container {}", &email);
            ctr.start(None).await.unwrap();
            let details = ctr.inspect().await.unwrap();
            let port = get_host_port(&details);
            Authority::try_from(format!("127.0.0.1:{}", port)).unwrap()
        }
        Err(e) => panic!("{:?}", e),
    };

    wait_for_http_service(authority.clone()).await;

    authority
}
