use std::fmt::{self, Formatter};

#[derive(Debug, Clone)]
pub enum DecodeTokenError {
    NoCookieHeader,
    ParseError,
    JWTError(jsonwebtoken::errors::Error),
}

impl fmt::Display for DecodeTokenError {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        use DecodeTokenError::*;
        match self {
            NoCookieHeader => write!(f, "No cookies present in headers"),
            ParseError => write!(f, "Unable to parse access_token cookie"),
            JWTError(err) => write!(f, "JWTError: {}", err),
        }
    }
}

impl std::error::Error for DecodeTokenError {}
